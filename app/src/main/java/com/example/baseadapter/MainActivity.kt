package com.example.baseadapter

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.baseadapter.adapters.NumbersRecyclerView
import com.example.baseadapter.databinding.ActivityMainBinding

class MainActivity : AppCompatActivity() {

    private lateinit var binding: ActivityMainBinding

    private val numbersList = mutableListOf<Int>()

    private lateinit var adapter: NumbersRecyclerView

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        addNumbers()
        init()
        setContentView(binding.root)
    }

    private fun init(){
        adapter = NumbersRecyclerView()
        binding.recyclerView.layoutManager = LinearLayoutManager(this)
        binding.recyclerView.adapter = adapter

        adapter.getData(numbersList)
    }



    private fun addNumbers(){
        for (i in 0..40){
            numbersList.add(i)
        }
    }
}