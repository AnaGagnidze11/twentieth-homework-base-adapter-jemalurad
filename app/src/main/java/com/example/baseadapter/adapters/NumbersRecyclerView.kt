package com.example.baseadapter.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import com.example.baseadapter.R


class NumbersRecyclerView: BaseAdapter<Int>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder<Int> {
        return NumberViewHolder(LayoutInflater.from(parent.context).inflate(R.layout.number_layout, parent, false))
    }

    inner class NumberViewHolder(itemView: View): ItemViewHolder<Int>(itemView){
        private val numberText = itemView.findViewById<TextView>(R.id.numberTxt)
        override fun onBind(item: Int) {
            numberText.text = item.toString()
        }
    }
}