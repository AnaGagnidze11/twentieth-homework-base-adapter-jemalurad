package com.example.baseadapter.adapters

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView

open class BaseAdapter<T> : RecyclerView.Adapter<ItemViewHolder<T>>(){

    private var items = emptyList<T>()

    fun getData(newItems: MutableList<T>){
        this.items = newItems
        notifyDataSetChanged()

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ItemViewHolder<T> {
        return ItemViewHolder(LayoutInflater.from(parent.context).inflate(viewType, parent, false))
    }

    override fun onBindViewHolder(holder: ItemViewHolder<T>, position: Int) {
        holder.onBind(items[position])
    }

    override fun getItemCount() = items.size
}

open class ItemViewHolder<T>(itemView: View): RecyclerView.ViewHolder(itemView), BindRecyclerViewHolder<T>{
    override fun onBind(item: T) {}

}
