package com.example.baseadapter.adapters

interface BindRecyclerViewHolder<T> {
    fun onBind(item: T)
}